﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ChartsAPI.Domain.Models;
using ChartsAPI.Domain.Interfaces;

namespace ChartsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChartController : ControllerBase
    {
        private readonly IChartService chartService;

        public ChartController(IChartService chartService)
        {
            this.chartService = chartService;
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<ActionResult<ChartDetails>> GetById(int id)
        {
            var chart = await chartService.GetById(id);
            if (chart == null)
            {
                return NotFound();
            }
            return Ok(chart);
        }

        [HttpGet]
        [Route("GetByTypeId")]
        public async Task<ActionResult<ChartDetails>> Get(int id)
        {
            var result = await chartService.GetByTypeId(id);
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("Get")]
        public async Task<ActionResult<ChartDetails>> Get()
        {
            var result = await chartService.GetAll();
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }
    }
}